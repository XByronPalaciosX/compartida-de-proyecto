/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Factory;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Home PC
 */
public final class MySQLConexionFactory extends ConexionDB{
        public MySQLConexionFactory(String[] criterios) {
        this.parametros = criterios; //para los criterios del array de la calse padra, parametro
        this.open();//llama al metodo open()
    }

    
    @Override
    Connection open() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            //en this.parametros[] iran los datos respectivos de la BD, usuario y clave
            this.conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + this.parametros[0], this.parametros[1], this.parametros[2]);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return this.conexion;
    }
}
